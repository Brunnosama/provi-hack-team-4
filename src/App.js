import HomePage from './HomePage';
import Header from './Header';

function App() {
  return (
    <div className="App">

      <Header />
        <HomePage/>

      {/* APP BODY */}
      {/* TAB CONTENT */}
    </div>
  );
}

export default App;
