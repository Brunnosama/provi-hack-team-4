import React from 'react'
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faBars } from '@fortawesome/free-solid-svg-icons';

const Header = () => {

    return (

        <MenuHeader>
            <ButtonHeader className="bttn_arrow" alt="botão voltar">
                <FontAwesomeIcon icon={faArrowLeft} />
            </ButtonHeader>
            <ButtonHeader className="bttn_menu" alt="botão menu">
                <FontAwesomeIcon icon={faBars} />
            </ButtonHeader>
        </MenuHeader>
    )
}

export default Header;

const MenuHeader = styled.header`
    display: flex;
    padding: 10px 25px 10px 25px;
    justify-content: space-between;
    position: sticky;
    z-index: 100;
    background-color: #0F5C7A;
    align-items: center;
    height: 60px;`

const ButtonHeader = styled.button`
    background: none;
    height: 30px;
    width: 30px;
    font-size: 18px;
    cursor: pointer;
    color: #FFFF;`