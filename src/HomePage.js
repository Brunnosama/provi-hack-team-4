import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';
import techCard from "./assets/img/tech-card.jpg"
import designCard from "./assets/img/design-card.jpg"
import manageCard from "./assets/img/manage-card.jpg"



const HomePage = () => {
    return (
        <>
            <div>
                <h3> Escolha qual será a sua trilha!</h3>
            </div>
            <div>
                <Container>
                    <figure className="position-relative">
                        <img src={techCard} alt="Trilha de Tecnologia da Informação" className="img-fluid" />
                        <Caption>
                            Tecnologia da Informação
                        </Caption>
                    </figure>
                </Container>
            </div>
            <div>
                <Container>
                    <figure className="position-relative">
                        <img src={designCard} alt="Trilha de Design" className="img-fluid" />
                        <Caption>
                            Design
                        </Caption>
                    </figure>
                </Container>
            </div>
            <div>
                <Container>
                    <figure className="position-relative">
                        <img src={manageCard} alt="Trilha de Gestão de Pessoas" className="img-fluid" />
                        <Caption>
                            Gestão de Pessoas
                        </Caption>
                    </figure>
                </Container>
            </div>

        </>
    )
}

export default HomePage;

const Caption = styled.figcaption`
    position: absolute;
    bottom: 2rem;
    margin: 0 2rem;
    color: #ffff;
    font-size: 1.5em;
    font-family: 'Roboto', sans-serif;
    font-weight: bold;
`;