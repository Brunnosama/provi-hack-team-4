import { createGlobalStyle } from "styled-components"

const GlobalStyled = createGlobalStyle`

    *{
        margin: 0;
        padding: 0;
        outline: 0;
        border: none;
        box-sizing: border-box;
    }

`;

export default GlobalStyled;